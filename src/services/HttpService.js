class Error404 extends Error {
  constructor () {
    super('Error 404')
  }
}

const serialize = (obj, prefix) => {
  let str = []
  for (let p in obj) {
    if (obj.hasOwnProperty(p)) {
      let k = prefix ? `${prefix}[${p}]` : p
      let v = obj[p]
      str.push((v !== null && typeof v === 'object')
        ? serialize(v, k)
        : encodeURIComponent(k) + '=' + encodeURIComponent(v))
    }
  }
  return str.join('&')
}

const getURL = (uri, queryObject) => {
  if (queryObject) {
    return `${uri}?${serialize(queryObject)}`
  }
  return uri
}

export default {
  async fetch (url, opts = {}, onProgress) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()
      xhr.open(opts.method || 'get', url, true)
      for (let k in opts.headers || {}) {
        xhr.setRequestHeader(k, opts.headers[k])
      }
      xhr.onload = e => resolve(e.target.responseText)
      xhr.onerror = reject
      if (xhr.upload && onProgress) {
        xhr.upload.onprogress = onProgress
      }
      xhr.send(opts.body)
    })
  },
  async getJSON (url, params) {
    const res = await this.fetch(getURL(url, params), {
      mode: 'no-cors', // Danger!!!
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      },
      credentials: 'same-origin'
    })
    // if (res.status === 404) {
    //   throw new Error404()
    // }
    return JSON.parse(res)  // res.json()
  },
  async postJSON (url, data) {
    const res = await this.fetch(url, {
      mode: 'no-cors', // Danger!!!
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
      credentials: 'same-origin'
    })
    // if (res.status === 404) {
    //   throw new Error404()
    // }
    return JSON.parse(res)  // res.json()
  },
  async postFile (url, file, onProgress) {
    const formData = new FormData()
    formData.append('file', file)
    let res = await this.fetch(url, {
      method: 'POST',
      body: formData
    }, onProgress)
    res = JSON.parse(res)
    if (res.status === 404) {
      throw new Error404()
    }
    return res
  }
}
