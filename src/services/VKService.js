import HttpService from './HttpService'

export default {
  async getInfo (id) {
    const response = await HttpService.getJSON(`${process.env.SCS_SERVER_URL}/api/vk`, {
      id: id
    })
    if (response.error) {
      throw new Error(response.error)
    }
    return response
  }
}
