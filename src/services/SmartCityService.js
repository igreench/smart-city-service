import HttpService from './HttpService'

export default {
  async getDataItem (id) {
    const response = await HttpService.getJSON(`${process.env.SCS_SERVER_URL}/api/data`, {
      id: id
    })
    if (response.error) {
      throw new Error(response.error)
    }
    return response
  },
  transformDataAll (items) {
    return items.map((item) => {
      return {
        identifier: item.identifier,
        title: item.title,
        organization: item.organization,
        organization_name: item.organization_name,
        topic: item.topic
      }
    })
  },
  async getDataAll (pageIndex, allPagesCount) {
    const response = await HttpService.getJSON(`${process.env.SCS_SERVER_URL}/api/data/all`, {
      pageIndex: pageIndex,
      allPagesCount: allPagesCount
    })
    if (response.error) {
      throw new Error(response.error)
    }
    return response  // this.transformDataAll(response)
  },
  async getDataByIds (ids) {
    const response = await HttpService.getJSON(`${process.env.SCS_SERVER_URL}/api/data/by_ids`, {
      ids: ids
    })
    if (response.error) {
      throw new Error(response.error)
    }
    return response  // this.transformDataAll(response)
  }
}
