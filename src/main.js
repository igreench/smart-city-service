// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Vuetify from 'vuetify'
import VueHighcharts from 'vue-highcharts'
import TreeView from 'vue-json-tree-view'
import App from './App'
import router from './router'
import nls from './nls/nls'
import('vuetify/dist/vuetify.min.css')

Vue.config.productionTip = false

Vue.use(VueI18n)
Vue.use(Vuetify)
Vue.use(VueHighcharts)
Vue.use(TreeView)

const i18n = new VueI18n({
  locale: 'en',
  messages: nls
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  template: '<App/>',
  components: {App}
})
