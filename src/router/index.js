import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/pages/HomePage'
import VKPage from '@/pages/VKPage'
import ExamplePage from '@/pages/ExamplePage'
import ExplorerItemPage from '@/pages/ExplorerItemPage'
import SettingsPage from '@/pages/SettingsPage'
import AboutPage from '@/pages/AboutPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/vk',
      name: 'VKPage',
      component: VKPage
    },
    {
      path: '/example',
      name: 'ExamplePage',
      component: ExamplePage
    },
    {
      path: '/explore',
      name: 'ExplorerItemPage',
      component: ExplorerItemPage
    },
    {
      path: '/settings',
      name: 'SettingsPage',
      component: SettingsPage
    },
    {
      path: '/about',
      name: 'AboutPage',
      component: AboutPage
    },
    {
      path: '*',
      redirect: {
        name: 'HomePage'
      }
    }
  ]
})
